TEST SCRAPER
=========================

Used technologies: Scrapy


test: scrapy crawl act -a searchterm="Colorado" -a items_count=100 -s LOG_FILE=~/1.log -o ~/1.json
test_without_json_file: scrapy crawl act -a items_count=10 -a searchterm='Colorado' -s LOG_FILE=~/1.log