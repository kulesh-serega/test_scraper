# -*- coding: utf-8 -*-
import urllib
import string
import time
import csv

import scrapy
from scrapy.http import Request
from scrapy.http import TextResponse
from selenium import webdriver
import googlemaps

from testcenter.items import TestcenterItem
from testcenter.settings import GOOGLE_API_KEY


is_empty = lambda x, y=None: x[0] if x else y


class ActSpider(scrapy.Spider):
    name = "act"
    allowed_domains = ["act.org"]

    SEARCH_PAGE_URL = 'https://www.act.org/content/act/en/' \
        'products-and-services/the-act/taking-the-test/' \
        'test-center-locator.html'

    def __init__(self, searchterm=None, items_count=10, *args, **kwargs):
        super(ActSpider, self).__init__(*args, **kwargs)
        self.items_count = int(items_count)
        self.searchterm = searchterm

    def start_requests(self):
        """Generate Requests from the SEARCH_PAGE_URL and the searchterm"""
        url_formatter = string.Formatter()
        yield Request(url_formatter.format(
            self.SEARCH_PAGE_URL,
            searchterm=urllib.quote_plus(self.searchterm.encode('utf-8')),))

    def parse(self, response):
        self.driver = webdriver.Firefox()
        self.driver.get(response.url)

        self.driver.find_element_by_xpath("//div[@id='state_chosen']").click()

        search_option = "//ul[@class='chosen-results']" \
            "/li[contains(text(), '%s')]" % self.searchterm
        self.driver.find_element_by_xpath(search_option).click()

        self.driver.find_element_by_xpath("//button[@id='testCentre']").click()

        time.sleep(5)

        while True:
            try:
                el_count = len(self.driver.find_elements_by_xpath(
                    "//div[@class='result-tile']"))

                if el_count < self.items_count:
                    element = self.driver.find_element_by_css_selector(
                        ".load-label a")
                    js_script = "arguments[0].click();"
                    self.driver.execute_script(js_script, element)
                else:
                    break
            except Exception as e:
                print 'Error: ', e
                break

        time.sleep(5)
        
        response = TextResponse(
            url=response.url,
            body=self.driver.page_source,
            encoding='utf-8')

        for school in response.xpath(
                "//div[@class='result-tile']")[:self.items_count]:
            item = TestcenterItem()
            item['city'] = self._get_city(school)
            item['name'] = self._get_name(school)
            item['code'] = self._get_code(school)
            item['test_dates'] = self._get_centerdates(school)
            item['address'] = self._get_address(item['name'])

            yield item

        self.driver.close()

    def _get_city(self, res):
        city = is_empty(
            res.xpath('.//div[@class="center-city"]/text()').extract())

        if city:
            return city

    def _get_name(self, res):
        name = is_empty(
            res.xpath('.//div[@class="center-name"]/text()').extract())

        if name:
            return name

    def _get_code(self, res):
        code = is_empty(res.xpath(
            './/ul[@class="center-code"]/li[last()]/text()').extract())

        if code:
            return code

    def _get_centerdates(self, res):
        dates = res.xpath('.//ul[@class="center-date"]/li/text()').extract()

        if dates:
            return ', '.join(date for date in dates[1:])

    def _get_address(self, center_name):        
        gmaps = googlemaps.Client(key=GOOGLE_API_KEY)
        result = gmaps.geocode(center_name)

        if result and 'formatted_address' in result[0]:
            return result[0]['formatted_address']
