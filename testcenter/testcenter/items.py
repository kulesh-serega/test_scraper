# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TestcenterItem(scrapy.Item):
    # define the fields for your item here like:
    city = scrapy.Field()
    name = scrapy.Field()
    code = scrapy.Field()
    test_dates = scrapy.Field()
    address = scrapy.Field()
